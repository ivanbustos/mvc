# Be aware that

- Due to time constraints, it was impossible for me to finish the frontend side on time.
- The backend, although it works, may be bloated with bugs as it is a custom solution which has not been tested thoroughly.
- To compensate the fact the frontend is not available, I am including an Insomnia file with example requests
for all available endpoints.
  
![insomnia](insomnia.png "Insomnia")

#Instructions

In order to build the project, make sure you have [Docker](https://www.docker.com/),  [Docker Compose](https://docs.docker.com/compose/) and [Git](https://git-scm.com/) installed.

1. Clone the site.

```shell
git clone https://gitlab.com/ivanbustos/mvc.git
```

2. Go into the recently created folder.

```shell
cd mvc
```

3. Build the environment

```shell
docker-compose build
```

4. Have Composer generate the Autoload file (No dependencies were installed.)

```shell
docker-compose run --rm composer install --ignore-platform-reqs
```

5. Have node download all frontend dependencies.

```shell
docker-compose run --rm node npm install
```

5. Run the project

```shell
docker-compose up -d
```

7. Head to http://localhost:8080/install and install the project by clicking on the installation button.

8. Head to http://localhost:8000/ and use the administration interface.
