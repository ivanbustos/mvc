<?php

use Mvc\Core\App;

require_once 'autoload.php';

$app = new App();
$app->start();
