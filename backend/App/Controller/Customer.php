<?php

namespace Mvc\App\Controller;

use Mvc\Core\Controller\EntityControllerBase;

/**
 * Class Customer
 *
 * @package Mvc\App\Controller
 */
class Customer extends EntityControllerBase
{

    /**
     * @inheritDoc
     */
    protected string $modelClass = \Mvc\App\Model\Customer::class;
}
