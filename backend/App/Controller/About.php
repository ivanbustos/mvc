<?php

namespace Mvc\App\Controller;

use Mvc\Core\Render\RenderableInterface;

/**
 * Class About
 *
 * @package Mvc\App\Controller
 */
class About implements RenderableInterface
{

    /**
     * @inheritDoc
     */
    public function build(): RenderableInterface
    {
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function render(): array
    {
        return [
        '#main_header' => 'MVC Demo developed by Iván Bustos for hhiselection.com.',
        '#secondary_header' => 'Please find more information at the README.md file.',
        ];
    }
}
