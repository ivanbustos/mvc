<?php

namespace Mvc\App\Controller;

use Mvc\Core\Controller\EntityControllerBase;

/**
 * Class Product
 *
 * @package Mvc\App\Controller
 */
class Product extends EntityControllerBase
{

    /**
     * @inheritDoc
     */
    protected string $modelClass = \Mvc\App\Model\Product::class;
}
