<?php

namespace Mvc\App\Controller;

use Mvc\Core\Database\Connection;
use Mvc\Core\Render\RenderableInterface;

/**
 * Class Installation
 *
 * @package Mvc\App\Controller
 */
class Installation implements RenderableInterface
{

    /**
     * The collection of known entities to install.
     *
     * @var array
     */
    protected array $entities = [];

    /**
     * The state of the installation.
     *
     * True for success, False otherwise.
     *
     * @var bool
     */
    protected bool $status;

    /**
     * Whether we are to show the installation form instead.
     *
     * @var bool
     */
    protected bool $isForm = false;

    /**
     * Installation constructor.
     */
    public function __construct()
    {
        $this->status = false;
        $this->entities[] = 'Mvc\App\Model\Product';
        $this->entities[] = 'Mvc\App\Model\Customer';
        $this->entities[] = 'Mvc\App\Model\Purchase';
    }

    /**
     * Performs the site wide installation.
     *
     * @return \Mvc\Core\Render\RenderableInterface
     */
    public function install(): RenderableInterface
    {
      // Assert connection.
        $connection = Connection::getInstance();
        if (!$connection->assertConnection()) {
            return $this;
        }
        foreach ($this->entities as $entity_class) {
            $class = new $entity_class();
            if (!call_user_func([$class, 'install'])) {
                return $this;
            }
        }
        $this->status = true;
        return $this;
    }

    /**
     * Presents the installation form.
     *
     * @return \Mvc\Core\Render\RenderableInterface
     */
    public function buildForm(): RenderableInterface
    {
        $this->isForm = true;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function render(): array
    {
        if ($this->isForm) {
            return [
            '#form' => true,
            '#main_header' => 'Installation.',
            '#instructions' => 'Just click on the bellow button to perform the installation.',
            ];
        }

        if (!$this->status) {
            return [
            '#form' => false,
            '#main_header' => 'Installation.',
            '#secondary_header' => 'There was a problem during installation. Is the Mariadb container up?',
            ];
        }
        return [
        '#form' => false,
        '#main_header' => 'Installation.',
        '#secondary_header' => 'Installation is Done!',
        ];
    }
}
