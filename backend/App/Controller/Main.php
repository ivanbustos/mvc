<?php

namespace Mvc\App\Controller;

use Mvc\Core\Render\RenderableInterface;

/**
 * Class Main
 *
 * @package Mvc\App\Controller
 */
class Main implements RenderableInterface
{

    /**
     * Default callback.
     *
     * @return \Mvc\Core\Render\RenderableInterface
     */
    public function build(): RenderableInterface
    {
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function render(): array
    {
        return [
        '#main_header' => 'Welcome to MVC Demo.',
        '#secondary_header' => 'Developed by Iván Bustos',
        ];
    }
}
