<?php

namespace Mvc\App\Controller;

use Mvc\Core\Controller\EntityControllerBase;

/**
 * Class Purchase
 *
 * @package Mvc\App\Controller
 */
class Purchase extends EntityControllerBase
{

    /**
     * @inheritDoc
     */
    protected string $modelClass = \Mvc\App\Model\Purchase::class;
}
