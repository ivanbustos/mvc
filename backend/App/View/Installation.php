<?php

namespace Mvc\App\View;

use Mvc\Core\Response\HtmlResponse;

/**
 * Class Installation
 *
 * @package Mvc\App\View
 */
class Installation extends HtmlResponse
{

    /**
     * @inheritDoc
     */
    public function getTemplateFile(): string
    {
        $render_array = $this->content->render();
        if ($render_array['#form']) {
            return __DIR__ . '/templates/installation.tpl';
        }
        return __DIR__ . '/templates/main.tpl';
    }
}
