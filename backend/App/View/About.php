<?php

namespace Mvc\App\View;

use Mvc\Core\Response\HtmlResponse;

/**
 * Class About
 *
 * @package Mvc\App\View
 */
class About extends HtmlResponse
{

    /**
     * @inheritDoc
     */
    public function getTemplateFile(): string
    {
        return __DIR__ . '/templates/main.tpl';
    }
}
