<?php

namespace Mvc\App\View;

use Mvc\Core\Response\HtmlResponse;

/**
 * Class Main
 *
 * @package Mvc\App\View
 */
class Main extends HtmlResponse
{

    /**
     * @inheritDoc
     */
    public function getTemplateFile(): string
    {
        return __DIR__ . '/templates/main.tpl';
    }
}
