<?php

namespace Mvc\App\Model;

use Mvc\Core\Model\EntityBase;
use Mvc\Core\Model\Fields\FieldFactory;
use Mvc\Core\Model\RenderableEntityTrait;
use Mvc\Core\Render\RenderableInterface;

/**
 * Class Product
 *
 * @package Mvc\App\Model
 */
class Product extends EntityBase implements RenderableInterface
{
    use RenderableEntityTrait;

    /**
     * Product constructor.
     *
     * @param array $data
     * @param bool $is_new
     */
    public function __construct(array $data = [], bool $is_new = true)
    {
        parent::__construct($data, $is_new);

        $this->fieldCollection->addField(FieldFactory::create(
            $this,
            'string',
            'title',
        ));

        $this->fieldCollection->addField(FieldFactory::create(
            $this,
            'string',
            'sku',
        ));

        $this->fieldCollection->addField(FieldFactory::create(
            $this,
            'money',
            'price',
        ));
    }

    /**
     * @inheritDoc
     */
    public static function getMachineName(): string
    {
        return 'product';
    }
}
