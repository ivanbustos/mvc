<?php

namespace Mvc\App\Model;

use Mvc\Core\Model\EntityBase;
use Mvc\Core\Model\Fields\FieldFactory;
use Mvc\Core\Model\RenderableEntityTrait;
use Mvc\Core\Render\RenderableInterface;

/**
 * Class Customer
 *
 * @package Mvc\App\Model
 */
class Customer extends EntityBase implements RenderableInterface
{
    use RenderableEntityTrait;

    /**
     * Customer constructor.
     *
     * @param array $data
     * @param bool $is_new
     */
    public function __construct(array $data = [], bool $is_new = true)
    {
        parent::__construct($data, $is_new);
        $this->fieldCollection->addField(FieldFactory::create(
            $this,
            'string',
            'name',
        ));

        $this->fieldCollection->addField(FieldFactory::create(
            $this,
            'string',
            'phone',
        ));

        $this->fieldCollection->addField(FieldFactory::create(
            $this,
            'string',
            'address',
        ));
    }

    /**
     * @inheritDoc
     */
    public static function getMachineName(): string
    {
        return 'customer';
    }
}
