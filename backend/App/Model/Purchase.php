<?php

namespace Mvc\App\Model;

use Mvc\Core\Model\EntityBase;
use Mvc\Core\Model\Fields\FieldFactory;
use Mvc\Core\Model\RenderableEntityTrait;
use Mvc\Core\Render\RenderableInterface;

/**
 * Class Purchase
 *
 * @package Mvc\App\Model
 */
class Purchase extends EntityBase implements RenderableInterface
{
    use RenderableEntityTrait;

    /**
     * Product constructor.
     *
     * @param array $data
     * @param bool $is_new
     */
    public function __construct(array $data = [], bool $is_new = true)
    {
        parent::__construct($data, $is_new);

        $this->fieldCollection->addField(FieldFactory::create(
            $this,
            'reference',
            'customer',
            [
            'target' => 'customer',
            'target_field' => 'id',
            ]
        ));

        $this->fieldCollection->addField(FieldFactory::create(
            $this,
            'reference',
            'product',
            [
            'target' => 'product',
            'target_field' => 'id',
            ]
        ));

        $this->fieldCollection->addField(FieldFactory::create(
            $this,
            'integer',
            'quantity',
        ));
    }

    /**
     * @inheritDoc
     */
    public static function getMachineName(): string
    {
        return 'purchase';
    }
}
