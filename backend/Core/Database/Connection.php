<?php

namespace Mvc\Core\Database;

/**
 * Singleton Connection
 *
 * @package Mvc\Core\Database
 */
final class Connection
{

    /**
     * The current connection handle.
     *
     * @var bool|\mysqli
     */
    private bool | \mysqli $connection;

    /**
     * An existent instance of this singleton.
     *
     * @var \Mvc\Core\Database\Connection|null
     */
    private static Connection | null $instance = null;

    /**
     * Connection constructor.
     */
    private function __construct()
    {
        $yml_file = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/App/database.yml');
        $config = yaml_parse($yml_file);
        $this->connection = mysqli_connect($config['host'], $config['user'], $config['pass'], $config['database']);
    }

    /**
     * Should not be called.
     */
    private function __clone()
    {
    }

    /**
     * Retrieves the existen singleton instance.
     *
     * @return \Mvc\Core\Database\Connection
     */
    public static function getInstance(): Connection
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Asserts whether a connection could be established.
     *
     * @return bool
     */
    public function assertConnection(): bool
    {
        if ($this->connection === false) {
            return false;
        }
        return true;
    }

    /**
     * Creates a table in the database.
     *
     * @param string $table_name
     * @param array $properties
     * @param string $primary
     *
     * @return bool
     */
    public function createTable(string $table_name, array $properties, string $primary): bool
    {
        array_walk($properties, function (&$value, $index) {
            $value = "`{$index}` {$value}, ";
        });
        $properties[] = "PRIMARY KEY (`$primary`)";
        $sql = "CREATE TABLE IF NOT EXISTS `" . $table_name . "` (" . implode('', $properties) . ");";
        return (bool) mysqli_query($this->connection, $sql);
    }

    /**
     * Retrieves data from the database via a SELECT query.
     *
     * @param string $table_name
     * @param string|null $key
     * @param mixed|null $value
     *
     * @return array|false
     */
    public function select(string $table_name, string $key = null, mixed $value = null): array | false
    {
        $sql = "SELECT * FROM `{$table_name}`";
        if ($key && $value) {
            if (!is_numeric($value)) {
                $value = "`{$value}`";
            }
            $sql .= " WHERE `{$key}`={$value} ";
        }
        $sql .= ';';
        if ($result = mysqli_query($this->connection, $sql)) {
            $result = $result->fetch_all(MYSQLI_ASSOC);
            if ($key && $value) {
                return reset($result);
            }
            return $result;
        }
        return false;
    }

    /**
     * Fetches data from the database via an UPDATE query.
     *
     * @param string $table_name
     * @param array $data
     * @param string $key
     * @param mixed $value
     *
     * @return bool
     */
    public function update(string $table_name, array $data, string $key, mixed $value): bool
    {
        array_walk($data, function (&$value, $index) {
            if (!is_numeric($value)) {
                $value = "'{$value}'";
            }
            $value = "`{$index}`={$value}";
        });
        $data = implode(', ', $data);

        if (!is_numeric($value)) {
            $value = "'{$value}'";
        }

        $sql = "UPDATE `{$table_name}` SET {$data} WHERE {$key}={$value};";

        if (mysqli_query($this->connection, $sql)) {
            return true;
        }
        return false;
    }

    /**
     * Inserts a single row in the database using an INSERT query.
     *
     * @param string $table_name
     * @param array $data
     *
     * @return int|false
     */
    public function insert(string $table_name, array $data): int | false
    {
        $props = array_keys($data);
        array_walk($props, function (&$value) {
            $value = "`{$value}`";
        });
        $props = implode(', ', $props);

        $values = array_values($data);
        array_walk($values, function (&$value) {
            if (is_numeric($value)) {
                return;
            }
            $value = "'{$value}'";
        });
        $values = implode(', ', $values);

        $sql = "INSERT INTO `{$table_name}` ({$props}) VALUES ({$values});";

        if (mysqli_query($this->connection, $sql)) {
            return mysqli_insert_id($this->connection);
        }
        return false;
    }

    /**
     * Deletes a single row in the database using a DELETE query.
     *
     * @param string $table_name
     * @param string $key
     * @param mixed $value
     *
     * @return bool
     */
    public function delete(string $table_name, string $key, mixed $value): bool
    {
        if (!is_numeric($value)) {
            $value = "'{$value}'";
        }
        $sql = "DELETE FROM `{$table_name}` WHERE `{$key}`={$value};";
        return mysqli_query($this->connection, $sql);
    }
}
