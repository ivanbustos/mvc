<?php

namespace Mvc\Core\Utils;

/**
 * Class Utils
 *
 * @package Mvc\Core\Utils
 */
class Utils
{

    /**
     * Creates a string of the type "$a::$b".
     *
     * @param string $a
     * @param string $b
     *
     * @return string
     */
    public static function concatenateWithDoubleColon(string $a, string $b): string
    {
        return "{$a}::{$b}";
    }
}
