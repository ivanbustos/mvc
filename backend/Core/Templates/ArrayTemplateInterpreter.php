<?php

namespace Mvc\Core\Templates;

use Mvc\Core\Render\RenderableInterface;

/**
 * Class ArrayTemplateInterpreter
 *
 * @package Mvc\Core\Templates
 */
class ArrayTemplateInterpreter implements TemplateInterpreterInterface
{

    /**
     * The data to interpret.
     *
     * @var array
     */
    protected array $data = [];

    /**
     * @inheritDoc
     */
    public function setData($data): void
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Data must be an array for this interpreter');
        }
        $this->data = $data;
    }

    /**
     * @inheritDoc
     */
    public function getInterpreted(string $template): string
    {
        foreach ($this->data as $needle => $replacement) {
            $template = str_replace("$needle", $replacement, $template);
        }
        return $template;
    }
}
