<?php

namespace Mvc\Core\Templates;

/**
 * Interface TemplateInterpreterInterface
 *
 * @package Mvc\Core\Templates
 */
interface TemplateInterpreterInterface
{

    /**
     * Sets the data to interpret.
     *
     * @param $data
     */
    public function setData($data): void;

    /**
     * Obtains the interpreted value.
     *
     * @param string $template
     *
     * @return string
     */
    public function getInterpreted(string $template): string;
}
