<?php

namespace Mvc\Core;

use Mvc\Core\Response\HttpResponseInterface;
use Mvc\Core\Response\JsonResponse;
use Mvc\Core\Utils\Utils;

/**
 * Class App
 *
 * @package Mvc\Core
 */
final class App
{

    /**
     * The set of found routes.
     *
     * @var array
     */
    private array $routes = [];

    /**
     * The current uri.
     *
     * @var string|mixed
     */
    private string $uri;

    /**
     * The request method in use.
     *
     * @var string
     */
    private string $requestMethod;

    /**
     * The document root.
     *
     * @var string|mixed
     */
    private string $documentRoot;

    /**
     * App constructor.
     */
    public function __construct()
    {
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->documentRoot = $_SERVER['DOCUMENT_ROOT'];
        $this->requestMethod = strtoupper($_SERVER['REQUEST_METHOD']);
        $this->parseRouting();
    }

    /**
     * Parses the App routing YAML file and populates the routes array.
     */
    private function parseRouting(): void
    {
        if ($this->routes) {
            return;
        }
        $yml_file = file_get_contents($this->documentRoot . '/App/routing.yml');
        $yml = yaml_parse($yml_file);

        $routes = [];
        foreach ($yml as $route_data) {
            $route_key = Utils::concatenateWithDoubleColon($route_data['path'], strtoupper($route_data['method']));
            $routes[$route_key] = [
            'title' => $route_data['title'],
            'class' => $route_data['class'],
            'callback' => $route_data['callback'],
            'view_class' => str_replace('Controller', 'View', $route_data['class']),
            ];
        }
        $this->routes = $routes;
    }

    /**
     * The 404 route used to serve all other requests.
     *
     * @return array
     */
    protected function get404Route(): array
    {
        return [
        'title' => '404',
        'class' => '\Mvc\Core\Controller\NotFound',
        'callback' => 'build',
        'view_class' => '\Mvc\Core\View\NotFound',
        ];
    }

    /**
     * Handles the OPTIONS CORS preflight.
     */
    protected function handlePreflight() {
        $response = new JsonResponse();
        $response->setStatusCode(200);
        $response->send();
    }

    /**
     * Given a route array, serves the appropiate response.
     *
     * @param array $route
     * @param int $status
     * @param array $post
     * @param array $parameter
     */
    protected function process(array $route, $status = 200, $post = [], $parameter = [])
    {
        if (!class_exists($route['class'])) {
            $this->process($this->get404Route(), 404);
            return;
        }
        $controller = new $route['class']();

      // Validate whether the view class exists and implements correct interface.
        if (!class_exists($route['view_class'])) {
            $this->process($this->get404Route(), 404);
            return;
        }
        $view = new $route['view_class']();
        if (!$view instanceof HttpResponseInterface) {
            $this->process($this->get404Route(), 404);
            return;
        }

        // Invoke the class and expect the result to be of type RenderableInterface.
        try {
            if ($parameter || $post) {
                if ($parameter && $post) {
                    $result = call_user_func([$controller, $route['callback']], $parameter, $post);
                } elseif ($parameter) {
                    $result = call_user_func([$controller, $route['callback']], $parameter);
                } else {
                    $result = call_user_func([$controller, $route['callback']], $post);
                }
            } else {
                $result = call_user_func([$controller, $route['callback']]);
            }
        } catch (\TypeError | \Exception $e) {
            $this->process($this->get404Route(), 404);
            return;
        }

        $view->setRenderableContent($result);
        $view->setStatusCode($status);
        $view->setTitle($route['title']);
        $view->send();
    }

    /**
     * Matches a route against the given request and calls ::process().
     */
    public function start()
    {
        if ($this->requestMethod == 'OPTIONS') {
            $this->handlePreflight();
            return;
        }

        // Find the route for the given uri.
        $parameter = null;
        $uri = $this->uri;
        if (preg_match('/(\d+$)/', $uri, $matches)) {
            $parameter = $matches[1];
            $uri = preg_replace('/(\d+$)/', '*', $this->uri);
        }

        // Assess whether the route exists.
        $route_key = Utils::concatenateWithDoubleColon($uri, $this->requestMethod);
        if (!isset($this->routes[$route_key])) {
          // Send the 404 page if not found.
            $this->process($this->get404Route(), 404);
            return;
        }
        $route = $this->routes[$route_key];

        // Attempt to find the post JSON object.
        $post = null;
        if (in_array($this->requestMethod, ['POST', 'PATCH'])) {
            $post = json_decode(file_get_contents('php://input'), true);
        }

        // Process the route.
        $this->process($route, 200, $post, $parameter);
    }
}
