<?php

namespace Mvc\Core\Model;

/**
 * Trait RenderableEntityTrait
 *
 * @package Mvc\Core\Model
 */
trait RenderableEntityTrait
{

    /**
     * Automatically creates a render array for an entity.
     *
     * @return array
     */
    public function render(): array
    {
        $render = [
        'type' => static::getMachineName(),
        ];
        foreach (array_keys($this->getEntityProperties()) as $property) {
            $render[$property] = (string) $this->get($property);
        }
        /** @var \Mvc\Core\Model\FieldInterface $field */
        foreach ($this->getFieldCollection() as $field) {
            $render[$field->getMachineName()] = (string) $field->getValue();
        }
        return $render;
    }
}
