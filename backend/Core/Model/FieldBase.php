<?php

namespace Mvc\Core\Model;

use Mvc\Core\Database\Connection;

/**
 * Class FieldBase
 *
 * @package Mvc\Core\Model
 */
abstract class FieldBase implements FieldInterface
{

    /**
     * Stores the name of the field.
     *
     * @var string
     */
    protected string $name;

    /**
     * Stores the autocalculated table name.
     *
     * @var string
     */
    protected string $tableName;

    /**
     * Stores the field value.
     *
     * @var mixed
     */
    protected mixed $value;

    /**
     * The current database connection.
     *
     * @var \Mvc\Core\Database\Connection
     */
    protected Connection $connection;

    /**
     * The entity to which this fields belongs.
     *
     * @var \Mvc\Core\Model\EntityInterface
     */
    protected EntityInterface $parent;

    /**
     * The field configuration.
     *
     * @var array|null
     */
    protected array | null $config;

    /**
     * FieldBase constructor.
     *
     * @param \Mvc\Core\Model\EntityInterface $parent
     * @param string $name
     * @param array|null $config
     */
    public function __construct(EntityInterface $parent, string $name, array $config = null)
    {
        $this->connection = Connection::getInstance();
        $this->parent = $parent;
        $this->name = $name;
        $this->tableName = $this->parent->getMachineName() . '__' . $name;
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    public function getPrimaryProperty(): string
    {
        return 'id';
    }

    protected function getFieldProperties(): array
    {
        return [
        'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
        'value' => $this->getValueSchema(),
        'parent' => 'INT(11) NOT NULL, FOREIGN KEY (parent) REFERENCES ' . $this->parent->getMachineName() . ' (' . $this->parent->getPrimaryProperty() .  ') ON DELETE CASCADE',
        ];
    }

    /**
     * @inheritDoc
     */
    public function getMachineName(): string
    {
        return $this->name;
    }

    /**
     * Inserts the field values.
     *
     * @return bool
     */
    protected function insert(): bool
    {
        return $this->connection->insert(
            $this->tableName,
            [
            'parent' => $this->parent->getId(),
            'value' => $this->getValue(),
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function load(): bool
    {
        $data = $this->connection->select(
            $this->tableName,
            'parent',
            $this->parent->getId(),
        );
        if (!$data) {
            return false;
        }
        $this->setValue($data['value']);
        return true;
    }

    /**
     * @inheritDoc
     */
    public function save(): bool
    {
        if ($this->parent->isNew) {
            return $this->insert();
        }
        return $this->update();
    }

    /**
     * Updates the field values.
     *
     * @return bool
     */
    protected function update(): bool
    {
        return $this->connection->update(
            $this->tableName,
            [
            'value' => $this->getValue(),
            ],
            'parent',
            $this->parent->getId(),
        );
    }

    /**
     * @inheritDoc
     */
    public function install(): bool
    {
        return $this->connection->createTable(
            $this->tableName,
            $this->getFieldProperties(),
            $this->getPrimaryProperty()
        );
    }

    /**
     * @inheritDoc
     */
    public function setValue(mixed $value): void
    {
        $this->value = $value;
    }

    /**
     * @inheritDoc
     */
    public function getValue(): mixed
    {
        return $this->value;
    }
}
