<?php

namespace Mvc\Core\Model;

use Mvc\Core\Database\Connection;

/**
 * A base class for all entities.
 *
 * @package Mvc\Core\Model
 */
abstract class EntityBase implements EntityInterface
{

    /**
     * The database connection.
     *
     * @var \Mvc\Core\Database\Connection
     */
    protected Connection $connection;

    /**
     * Whether the entity is new.
     *
     * @var bool|mixed
     */
    public bool $isNew;

    /**
     * The list of entity properties.
     *
     * @var array
     */
    protected array $properties;

    /**
     * The list of entity fields.
     *
     * @var \Mvc\Core\Model\FieldCollection
     */
    protected FieldCollection $fieldCollection;

    /**
     * EntityBase constructor.
     *
     * @param array $data
     * @param bool $is_new
     */
    public function __construct(array $data = [], $is_new = true)
    {
        $this->isNew = $is_new;
        $this->connection = Connection::getInstance();
        $this->fieldCollection = new FieldCollection();
        foreach ($data as $key => $value) {
            $this->properties[$key] = $value;
        }
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->properties['id'];
    }

    /**
     * @inheritDoc
     */
    public static function getPrimaryProperty(): string
    {
        return 'id';
    }

    /**
     * @inheritDoc
     */
    public function getFieldCollection(): FieldCollection
    {
        return $this->fieldCollection;
    }

    /**
     * @inheritDoc
     */
    public function getEntityProperties(): array
    {
        return [
        'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
        'created' => 'INT(11) NOT NULL',
        'changed' => 'INT(11) NOT NULL',
        ];
    }

    /**
     * Updates the entity.
     *
     * @return bool
     */
    protected function update(): bool
    {
        $this->set('changed', time());
        $result = $this->connection->update(
            static::getMachineName(),
            [
            'changed' => $this->get('changed'),
            ],
            static::getPrimaryProperty(),
            $this->getId()
        );
        if (!$result) {
            return false;
        }
        if ($this->saveFields()) {
            return true;
        }
        return false;
    }

    /**
     * Saves all entity fields.
     *
     * @return bool
     */
    protected function saveFields(): bool
    {
       /** @var \Mvc\Core\Model\FieldInterface $field */
        foreach ($this->getFieldCollection() as $field) {
            if (!$field->save()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Inserts a new entity.
     *
     * @return bool
     */
    protected function insert(): bool
    {
        $this->set('created', time());
        $this->set('changed', time());
        $result = $this->connection->insert(
            static::getMachineName(),
            [
            'created' => $this->get('created'),
            'changed' => $this->get('changed'),
            ]
        );
        if (!$result) {
            return false;
        }
        $this->set('id', $result);
        if ($this->saveFields()) {
            $this->isNew = false;
            return true;
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function save(): bool
    {
        if ($this->isNew) {
            return $this->insert();
        }
        return $this->update();
    }

    /**
     * @inheritDoc
     */
    public function install(): bool
    {
        // Install entity.
        $success = $this->connection->createTable(
            static::getMachineName(),
            $this->getEntityProperties(),
            static::getPrimaryProperty()
        );
        if (!$success) {
            return false;
        }

        /** @var \Mvc\Core\Model\FieldInterface $field */
        foreach ($this->getFieldCollection() as $field) {
            if (!$field->install()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Sets either a property or a field with the provided value.
     *
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, mixed $value)
    {
      /** @var \Mvc\Core\Model\FieldInterface $field */
        foreach ($this->getFieldCollection() as $field) {
            if ($field->getMachineName() == $key) {
                $field->setValue($value);
                return;
            }
        }
        $this->properties[$key] = $value;
    }

    /**
     * Gets the value of a property or a field.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key)
    {
      /** @var \Mvc\Core\Model\FieldInterface $field */
        foreach ($this->getFieldCollection() as $field) {
            if ($field->getMachineName() == $key) {
                return $field->getValue();
            }
        }
        return $this->properties[$key];
    }

    /**
     * @inheritDoc
     */
    public static function load(int $id): EntityInterface | false
    {
        $connection = Connection::getInstance();
        $result = $connection->select(static::getMachineName(), static::getPrimaryProperty(), $id);
        if ($result) {
            $entity = new static($result, false);
            foreach ($entity->getFieldCollection() as $field) {
                $field->load();
            }
            return $entity;
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public static function delete(int $id): bool
    {
        $connection = Connection::getInstance();
        return $connection->delete(static::getMachineName(), static::getPrimaryProperty(), $id);
    }

    /**
     * @inheritDoc
     */
    public static function loadMultiple(): EntityCollection
    {
        $collection = new EntityCollection();
        $connection = Connection::getInstance();
        $results = $connection->select(static::getMachineName());
        foreach ($results as $data) {
            $entity = new static($data, false);
            foreach ($entity->getFieldCollection() as $field) {
                $field->load();
            }
            $collection->addEntity($entity);
        }
        return $collection;
    }
}
