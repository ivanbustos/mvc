<?php

namespace Mvc\Core\Model;

/**
 * Interface FieldInterface
 *
 * @package Mvc\Core\Model
 */
interface FieldInterface
{

    /**
     * Set a value for this field.
     *
     * @param mixed $value
     */
    public function setValue(mixed $value): void;

    /**
     * Returns whatever value the field currently has.
     *
     * @return mixed
     */
    public function getValue(): mixed;

    /**
     * The name of the primary table key.
     *
     * @return string
     */
    public function getPrimaryProperty(): string;

    /**
     * The MySQL schema to use to create the value field.
     *
     * @return string
     */
    public function getValueSchema(): string;

    /**
     * The machine name of the field.
     *
     * @return string
     */
    public function getMachineName(): string;

    /**
     * Handles automated installation.
     *
     * @return bool
     */
    public function install(): bool;

    /**
     * Loads the field data.
     *
     * @return bool
     */
    public function load(): bool;

    /**
     * Saves the field.
     *
     * @return bool
     */
    public function save(): bool;
}
