<?php

namespace Mvc\Core\Model;

/**
 * Class FieldCollection
 *
 * @package Mvc\Core\Model
 */
class FieldCollection implements \Countable, \Iterator
{

    /**
     * The current set of fields.
     *
     * @var array
     */
    private array $fields = [];

    /**
     * The current index.
     *
     * @var int
     */
    private int $currentIndex = 0;

    /**
     * Adds a field to the collection.
     *
     * @param \Mvc\Core\Model\FieldInterface $field
     */
    public function addField(FieldInterface $field)
    {
        $this->fields[] = $field;
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->fields[$this->currentIndex];
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        $this->currentIndex++;
    }

    /**
     * @inheritDoc
     */
    public function key(): int
    {
        return $this->currentIndex;
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return isset($this->fields[$this->currentIndex]);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->currentIndex = 0;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->fields);
    }
}
