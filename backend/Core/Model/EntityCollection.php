<?php

namespace Mvc\Core\Model;

use Mvc\Core\Render\RenderableInterface;

/**
 * Class EntityCollection
 *
 * @package Mvc\Core\Model
 */
class EntityCollection implements \Countable, \Iterator, RenderableInterface
{

    /**
     * The list of given entities.
     *
     * @var array
     */
    private array $entities = [];

    /**
     * The current index.
     *
     * @var int
     */
    private int $currentIndex = 0;

    /**
     * Adds an entity to the collection.
     *
     * @param \Mvc\Core\Model\EntityInterface $entity
     */
    public function addEntity(EntityInterface $entity)
    {
        $this->entities[] = $entity;
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->entities[$this->currentIndex];
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        $this->currentIndex++;
    }

    /**
     * @inheritDoc
     */
    public function key(): int
    {
        return $this->currentIndex;
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return isset($this->entities[$this->currentIndex]);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->currentIndex = 0;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->entities);
    }

    /**
     * @inheritDoc
     */
    public function render(): array
    {
        $render = [];

        foreach ($this->entities as $entity) {
            if ($entity instanceof RenderableInterface) {
                $render[] = $entity->render();
            }
        }

        return $render;
    }
}
