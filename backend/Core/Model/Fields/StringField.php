<?php

namespace Mvc\Core\Model\Fields;

use Mvc\Core\Model\FieldBase;

/**
 * Class StringField
 *
 * @package Mvc\Core\Model\Fields
 */
class StringField extends FieldBase
{

    /**
     * @inheritDoc
     */
    public function getValueSchema(): string
    {
        return 'VARCHAR(255) NOT NULL';
    }
}
