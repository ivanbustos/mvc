<?php

namespace Mvc\Core\Model\Fields;

use Mvc\Core\Model\EntityInterface;

/**
 * Factory class for corresponding fields.
 *
 * @package Mvc\Core\Model\Fields
 */
class FieldFactory
{

    /**
     * Factory method creates a field of the provided type.
     *
     * @param \Mvc\Core\Model\EntityInterface $parent
     * @param string $type
     * @param string $name
     * @param array|null $config
     *
     * @return mixed
     */
    public static function create(
        EntityInterface $parent,
        string $type,
        string $name,
        array $config = null
    ) {
        $class = __NAMESPACE__ . '\\' . ucfirst(strtolower($type)) . 'Field';
        return new $class($parent, $name, $config);
    }
}
