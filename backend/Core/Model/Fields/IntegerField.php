<?php

namespace Mvc\Core\Model\Fields;

use Mvc\Core\Model\FieldBase;

/**
 * Class IntegerField
 *
 * @package Mvc\Core\Model\Fields
 */
class IntegerField extends FieldBase
{

    /**
     * @inheritDoc
     */
    public function getValueSchema(): string
    {
        return 'INT(11) NOT NULL';
    }
}
