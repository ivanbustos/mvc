<?php

namespace Mvc\Core\Model\Fields;

use Mvc\Core\Model\FieldBase;

/**
 * Class ReferenceField
 *
 * @package Mvc\Core\Model\Fields
 */
class ReferenceField extends FieldBase
{

    /**
     * @inheritDoc
     */
    public function getValueSchema(): string
    {
        return 'INT(11) NOT NULL, FOREIGN KEY (value) REFERENCES ' . $this->config['target'] . ' (' . $this->config['target_field'] .  ') ON DELETE CASCADE';
    }
}
