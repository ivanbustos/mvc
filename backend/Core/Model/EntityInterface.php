<?php

namespace Mvc\Core\Model;

/**
 * Interface EntityInterface
 *
 * @package Mvc\Core\Model
 */
interface EntityInterface
{

    /**
     * ID accessor.
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Saves the entity.
     *
     * @return bool
     */
    public function save(): bool;

    /**
     * Deletes an entity given an id.
     *
     * @param int $id
     *
     * @return bool
     */
    public static function delete(int $id): bool;

    /**
     * Loads the entity given an id.
     *
     * @param int $id
     *
     * @return \Mvc\Core\Model\EntityInterface|false
     */
    public static function load(int $id): EntityInterface | false;

    /**
     * Loads all existing entities of this type.
     *
     * @return \Mvc\Core\Model\EntityCollection
     */
    public static function loadMultiple(): EntityCollection;

    /**
     * Retrieves the list of files attached.
     *
     * @return \Mvc\Core\Model\FieldCollection
     */
    public function getFieldCollection(): FieldCollection;

    /**
     * Retrieves the main table key.
     *
     * @return string
     */
    public static function getPrimaryProperty(): string;

    /**
     * Retrieves the list of entity properties.
     *
     * @return array
     */
    public function getEntityProperties(): array;

    /**
     * Retrieves the entity machine name.
     *
     * @return string
     */
    public static function getMachineName(): string;

    /**
     * Performs automated installation.
     *
     * @return bool
     */
    public function install(): bool;
}
