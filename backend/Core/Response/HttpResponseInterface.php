<?php

namespace Mvc\Core\Response;

use Mvc\Core\Render\RenderableInterface;

/**
 * Interface HttpResponseInterface
 *
 * @package Mvc\Core\Response
 */
interface HttpResponseInterface
{

    /**
     * Content type mutator.
     *
     * @param string $content_type
     */
    public function setContentType(string $content_type): void;

    /**
     * Title accessor.
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Title mutator.
     *
     * @param string $title
     */
    public function setTitle(string $title): void;

    /**
     * Renderable content mutator.
     *
     * @param \Mvc\Core\Render\RenderableInterface $content
     */
    public function setRenderableContent(RenderableInterface $content): void;

    /**
     * Renderable content accessor.
     *
     * @return \Mvc\Core\Render\RenderableInterface
     */
    public function getRenderableContent(): RenderableInterface;

    /**
     * Retrieves the rendered content.
     *
     * @return string
     */
    public function getRenderedContent(): string;

    /**
     * Status code mutator.
     *
     * @param int $status_code
     */
    public function setStatusCode(int $status_code): void;

    /**
     * Sends the response to client.
     */
    public function send(): void;
}
