<?php

namespace Mvc\Core\Response;

use Mvc\Core\Render\RenderableInterface;
use Mvc\Core\Templates\TemplateInterpreterInterface;

abstract class HttpResponseBase implements HttpResponseInterface
{

    /**
     * The response status code.
     *
     * @var int
     */
    protected int $statusCode;

    /**
     * The content to print.
     *
     * @var \Mvc\Core\Render\RenderableInterface
     */
    protected RenderableInterface $content;

    /**
     * The headers to send along with the response.
     *
     * @var array
     */
    protected array $headers = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => 'GET, POST, PATCH, DELETE, OPTIONS',
        'Access-Control-Allow-Headers' => 'content-type'
    ];

    /**
     * The interpreter to use to interpret HTML against a render array.
     *
     * @var \Mvc\Core\Templates\TemplateInterpreterInterface
     */
    protected TemplateInterpreterInterface $interpreter;

    /**
     * The title to print in the template file.
     *
     * @var string
     */
    protected string $title;

    /**
     * HttpResponseBase constructor.
     */
    public function __construct()
    {
        $date = new \DateTimeImmutable();
        $headers['Date'] = $date->format('D, d M Y H:i:s') . ' GMT';
        $headers['pragma'] = 'no-cache';
    }

    /**
     * @inheritDoc
     */
    public function setRenderableContent(RenderableInterface $content): void
    {
        $this->content = $content;
    }

    /**
     * @inheritDoc
     */
    public function getRenderableContent(): RenderableInterface
    {
        return $this->content;
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * Sends the generated string over to the client.
     */
    protected function sendContent(): void
    {
        echo $this->getRenderedContent();
    }

    /**
     * Prepares the response by attaching response headers.
     */
    protected function sendHeaders()
    {
        foreach ($this->headers as $name => $value) {
            header($name . ': ' . $value, true, $this->statusCode);
        }
    }

    /**
     * @inheritDoc
     */
    public function setContentType(string $content_type): void
    {
        $this->headers['Content-Type'] = $content_type;
    }

    /**
     * @inheritDoc
     */
    public function setStatusCode(int $status_code): void
    {
        $this->statusCode = $status_code;
    }

    /**
     * @inheritDoc
     */
    public function send(): void
    {
        $this->sendHeaders();
        $this->sendContent();
    }
}
