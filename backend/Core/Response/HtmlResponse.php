<?php

namespace Mvc\Core\Response;

use Mvc\Core\Templates\ArrayTemplateInterpreter;
use Mvc\Core\Templates\TemplateInterpreterInterface;

/**
 * Class HtmlResponse
 *
 * @package Mvc\Core\Response
 */
abstract class HtmlResponse extends HttpResponseBase implements HtmlResponseInterface
{

    /**
     * @inheritDoc
     */
    public function setTemplateInterpreter(TemplateInterpreterInterface $interpreter)
    {
        $this->interpreter = $interpreter;
    }

    /**
     * HtmlResponse constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setContentType('text/html');

      // Set a default template interpreter.
        $interpreter = new ArrayTemplateInterpreter();
        $this->setTemplateInterpreter($interpreter);
    }

    /**
     * @inheritDoc
     */
    public function getRenderedContent(): string
    {
        $content_template = file_get_contents($this->getTemplateFile());
        $this->interpreter->setData($this->getRenderableContent()->render());
        $interpreter = new ArrayTemplateInterpreter();
        $interpreter->setData([
        '#title' => $this->getTitle(),
        '#content' => $this->interpreter->getInterpreted($content_template),
        ]);
        $outer_template = file_get_contents(__DIR__ . '/templates/html.tpl');
        return $interpreter->getInterpreted($outer_template);
    }
}
