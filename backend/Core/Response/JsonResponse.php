<?php

namespace Mvc\Core\Response;

use Mvc\Core\Templates\ArrayTemplateInterpreter;
use Mvc\Core\Templates\TemplateInterpreterInterface;

class JsonResponse extends HttpResponseBase
{

    public function __construct()
    {
        parent::__construct();
        $this->setContentType('Application/Json');
    }

    public function getRenderedContent(): string
    {
        return json_encode($this->getRenderableContent()->render());
    }
}
