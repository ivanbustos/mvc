<?php

namespace Mvc\Core\Response;

use Mvc\Core\Templates\TemplateInterpreterInterface;

/**
 * Interface HtmlResponseInterface
 *
 * @package Mvc\Core\Response
 */
interface HtmlResponseInterface
{

    /**
     * The location of the template file to use for rendering.
     *
     * @return string
     */
    public function getTemplateFile(): string;

    /**
     * Sets a default Interpreter class to process array values against HTML.
     *
     * @param \Mvc\Core\Templates\TemplateInterpreterInterface $interpreter
     */
    public function setTemplateInterpreter(TemplateInterpreterInterface $interpreter);
}
