<?php

namespace Mvc\Core\Controller;

use Mvc\Core\Model\EntityInterface;
use Mvc\Core\Render\RenderableInterface;

/**
 * This the controller base class Entity controllers should use.
 *
 * @package Mvc\Core\Controller
 */
abstract class EntityControllerBase implements RenderableInterface
{

    /**
     * The model class being used.
     *
     * @var string
     */
    protected string $modelClass;

    /**
     * Obtains the list of fields from an entity.
     *
     * @param \Mvc\Core\Model\EntityInterface $entity
     *
     * @return array
     */
    protected function getModelFields(EntityInterface $entity): array
    {
      /** @var \Mvc\Core\Model\EntityInterface $entity */
        $props = [];

      /** @var \Mvc\Core\Model\FieldInterface $field */
        foreach ($entity->getFieldCollection() as $field) {
            $props[] = $field->getMachineName();
        }

        return $props;
    }

    /**
     * Creates a new object of the current entity type.
     *
     * @param array $data
     *
     * @return \Mvc\Core\Render\RenderableInterface
     */
    public function create(array $data): RenderableInterface
    {
        $entity = new $this->modelClass();
        $props = $this->getModelfields($entity);
        foreach ($props as $prop) {
            if (isset($data[$prop])) {
                $entity->set($prop, $data[$prop]);
            }
        }
        $entity->save();
        return $entity;
    }

    /**
     * Returns a collection of entities.
     *
     * @return \Mvc\Core\Render\RenderableInterface
     */
    public function collection(): RenderableInterface
    {
        return call_user_func($this->modelClass . '::loadMultiple');
    }

    /**
     * Returns a single entity view.
     *
     * @param int $id
     *
     * @return \Mvc\Core\Render\RenderableInterface
     */
    public function single(int $id): RenderableInterface
    {
        return call_user_func($this->modelClass . '::load', $id);
    }

    /**
     * Updates a single entity.
     *
     * @param int $id
     * @param array $data
     *
     * @return \Mvc\Core\Render\RenderableInterface
     */
    public function update(int $id, array $data): RenderableInterface
    {
        $entity = call_user_func($this->modelClass . '::load', $id);
        $props = $this->getModelfields($entity);
        foreach ($props as $prop) {
            if (isset($data[$prop])) {
                $entity->set($prop, $data[$prop]);
            }
        }
        $entity->save();
        return $entity;
    }

    /**
     * Deletes a single entity.
     *
     * @param int $id
     *
     * @return $this
     */
    public function delete(int $id): self
    {
        call_user_func($this->modelClass . '::delete', $id);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function render(): array
    {
        return [
        'ok',
        ];
    }
}
