<?php

namespace Mvc\Core\Controller;

use Mvc\Core\Render\RenderableInterface;

/**
 * Class NotFound
 *
 * @package Mvc\Core\Controller
 */
class NotFound implements RenderableInterface
{

    /**
     * Default controller callback.
     *
     * @return \Mvc\Core\Render\RenderableInterface
     */
    public function build(): RenderableInterface
    {
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function render(): array
    {
        return [
        '#main_header' => '404',
        '#secondary_header' => 'Page not found!',
        ];
    }
}
