<?php

namespace Mvc\Core\Render;

/**
 * Interface RenderableInterface
 *
 * @package Mvc\Core\Render
 */
interface RenderableInterface
{

    /**
     * Returns a renderable array that can be
     * taken by a view.
     *
     * @return array
     */
    public function render(): array;
}
