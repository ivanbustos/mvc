<?php

namespace Mvc\Core\View;

use Mvc\Core\Response\HtmlResponse;

/**
 * Class NotFound
 *
 * @package Mvc\Core\View
 */
class NotFound extends HtmlResponse
{

    /**
     * @inheritDoc
     */
    public function getTemplateFile(): string
    {
        return __DIR__ . '/templates/not-found.tpl';
    }
}
