(function($, axios) {

  const updateCustomers = function() {
    axios.get('http://localhost:8080/api/products')
      .then(function(result) {
        const data = result.data;
        const $tbody = $('#nav-products table tbody');
        $tbody.empty();
        $.each(data, function(index, value) {
          const $tr = $('<tr>');
          $tr.append($('<td>').html(value.id));
          $tr.append($('<td>').html(value.sku));
          $tr.append($('<td>').html(value.title));
          $tr.append($('<td>').html('COP $' + value.price));
          const created = new Date(value.created * 1000);
          $tr.append($('<td>').html(created.toUTCString()));
          const changed = new Date(value.changed * 1000);
          $tr.append($('<td>').html(changed.toUTCString()));
          $tr.appendTo($tbody);
        });
      })
  };

  updateCustomers();

  $('#products-modal form .btn-primary').click(function(evt) {
    evt.preventDefault();
    const $parent = $(this).parent();
    if ($parent[0].checkValidity()) {
      const title = $('#products-title-input', $parent).val();
      const sku = $('#products-sku-input', $parent).val();
      const price = $('#products-price-input', $parent).val();
      axios.post('http://localhost:8080/api/products', {
        'title': title,
        'sku': sku,
        'price': price,
      })
      .then(function(result) {
        updateCustomers();
      })
    }
  });
})(jQuery, axios);
